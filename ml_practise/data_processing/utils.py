import pathlib
from typing import Dict
from matplotlib import pyplot as plt
import pandas as pd
from ml_practise import config

path = pathlib.Path(__file__).parent.parent.resolve()


class Utils:
    def __init__(self) -> None:
        pass

    def store_estimator_results(self, cv_results_: Dict) -> None:
        print("Estimator values saving to csv started")
        cv_results = pd.DataFrame(cv_results_)[
            [
                "params",
                "mean_test_score",
                "std_test_score",
                "rank_test_score",
                "mean_train_score",
                "std_train_score",
            ]
        ]
        cv_results.to_csv(
            (path / f"estimation/{config['DATASET']}_estimator_values.csv"),
            sep=",",
            encoding="utf-8",
        )
        print(
            f"Estimator values saving completed successfully at: "
            f"estimation/{config['DATASET']}_estimator_values.csv",
        )

    def view_model(
        self,
        y_actual: pd.DataFrame,
        y_predicted: pd.DataFrame,
        regressor_name: str,
    ) -> None:
        print("Model graph creation started")
        plt.plot(
            list(range(len(y_actual))), y_actual, color="g", label="actual"
        )
        plt.plot(
            list(range(len(y_actual))),
            y_predicted,
            color="b",
            label="predicted",
        )
        plt.legend()
        plt.savefig(
            path / f"estimation/{regressor_name}_view_model.png",
            bbox_inches="tight",
        )
        print(
            f"Model graph creation ended successfully at: "
            f"estimation/{regressor_name}_view_model.png"
        )
