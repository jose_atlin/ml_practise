from typing import List
import numpy as np
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import SimpleImputer, IterativeImputer
from sklearn.model_selection import RandomizedSearchCV
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder

from ml_practise.data_processing.dataframe_selector import DataFrameSelector


class Pipelining:
    def __init__(self) -> None:
        pass

    def regression_pipeline(
        self, numericals: List, categoricals: List
    ) -> RandomizedSearchCV:

        num_pipeline = Pipeline(
            [
                ("num_selector", DataFrameSelector(numericals)),
                ("num_imp", IterativeImputer()),
                ("num_sc", MinMaxScaler()),
            ]
        )
        print(f"value of `num_pipeline` is `{num_pipeline}`")

        cat_pipeline = Pipeline(
            [
                ("cat_selector", DataFrameSelector(categoricals)),
                (
                    "cat_imp",
                    SimpleImputer(
                        missing_values=np.nan, strategy="most_frequent"
                    ),
                ),
                ("cat_enc", OneHotEncoder()),
            ]
        )
        print(f"value of `cat_pipeline` is `{cat_pipeline}`")

        full_pipeline = FeatureUnion(
            transformer_list=[
                ("num_pipeline", num_pipeline),
                ("cat_pipeline", cat_pipeline),
            ]
        )
        print(
            f"value of `full_pipeline` is `{full_pipeline}`\n"
            "Models hyper-parameter tuning started"
        )
        return full_pipeline
