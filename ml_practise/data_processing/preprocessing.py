from typing import Dict, List, Tuple
import pandas as pd
import numpy as np
from sklearn import model_selection


class PreProcessing:
    def __init__(self, data: pd.DataFrame, target: str) -> None:
        self.data = data
        self.target = target

    def get_categorical_cols(self):
        categorical = self.data.select_dtypes(include=[object]).columns.tolist()
        if self.target in categorical:
            categorical.remove(self.target)
        return categorical

    def get_numerical_cols(self):
        numerical = self.data.select_dtypes(
            include=[np.number]
        ).columns.tolist()
        if self.target in numerical:
            numerical.remove(self.target)
        return numerical

    def get_none_val_cols(self) -> List:
        return self.data.columns[self.data.isna().any()].tolist()

    def get_ordered_categorical_cols(self) -> Tuple:
        cols: List = ["children"]
        labels: List = [[0, 1, 2, 3, 4, 5]]
        return cols, labels

    def split_data(self) -> Tuple:
        X = self.data.drop([self.target], axis=1)
        y = self.data[self.target]
        return X, y

    def train_test_split(self):
        X, y = self.split_data()
        return model_selection.train_test_split(X, y, shuffle=True)

    def column_headers_without_target(self) -> List:
        column_names = list(self.data.columns)
        column_names.remove(self.target)
        return column_names

    def convert_to_pd_df(self, X_new: Dict) -> pd.DataFrame:
        column_names = self.column_headers_without_target()
        dummy = {key: np.NaN for key in column_names}
        for key, value in X_new.items():
            if str(value).lower() not in ["none", "nan"]:
                dummy[key] = value
        return pd.DataFrame.from_records([dummy])
