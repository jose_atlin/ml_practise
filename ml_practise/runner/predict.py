import requests
import csv
import ast
from ml_practise.runner import path
from ml_practise import config


with open(
    path / f"data/{config['DATASET']}_predict.csv", newline=""
) as csv_file:

    csv_reader = csv.DictReader(csv_file, delimiter=",")

    for row in csv_reader:
        for key, value in row.items():
            try:
                row[key] = ast.literal_eval(value)
            except Exception:
                pass

        response = requests.post(
            url=f"http://localhost:{config['FLASK_PORT']}/{config['DATASET']}/predict",
            json=row,
        )
        print(
            f"Data Sent: `{row}`\n"
            f"Status Code : `{response.status_code}`\n"
            f"Reason: `{response.reason}`\n"
            f"Content: `{response.text}`"
        )
