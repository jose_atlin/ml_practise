import requests
from ml_practise import config


response = requests.get(
    url=f"http://localhost:{config['FLASK_PORT']}/{config['DATASET']}/train"
)
print(
    f"Status Code : `{response.status_code}`\n"
    f"Reason: `{response.reason}`\n"
    f"Content: `{response.text}`"
)
