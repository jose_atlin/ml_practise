from typing import Tuple
import pandas as pd
from ml_practise.models.regression import Regression
from ml_practise.runner import path
from ml_practise import config


class Initializer:
    def __init__(self) -> None:
        self.data = pd.read_csv(path / f"data/{config['DATASET']}_train.csv")
        self.target = config["TARGET"]

        # 1st heirarchy
        self.learning_category = config["LEARNING_CATEGORY"]

        # 2nd heirarchy
        self.learning_type = config["LEARNING_TYPE"]

        # 3rd heirarchy
        self.regression = Regression(self.data, self.target)

    def train(self) -> Tuple:
        return getattr(self, self.learning_type).train()

    def predict(self, data) -> Tuple:
        return getattr(self, self.learning_type).predict(data)
