import json
import pathlib
from warnings import simplefilter
from sklearn.exceptions import FitFailedWarning, ConvergenceWarning

simplefilter("ignore", (FitFailedWarning, ConvergenceWarning, UserWarning))

path = pathlib.Path(__file__).parent.resolve()

with open(path / "config.json", "r") as fp:
    config = json.load(fp)
