import time
import pathlib


path = pathlib.Path(__file__).parent.resolve()


def request_wrapper(func):
    def inner_func(*args, **kwargs):
        start_t = time.perf_counter()
        response_data = func(*args, **kwargs)
        end_t = time.perf_counter()

        response_data.update({"elapsed_time": "%.5f" % (end_t - start_t)})
        return response_data

    inner_func.__name__ = func.__name__
    return inner_func
