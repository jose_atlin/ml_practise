let operation = "";

function setAlert(alertClass, alertType, alertMessage) {
    const template = $("#template-alert").html();
    $("#custom-alert").html(template);
    $("#alert").prop("class", alertClass);
    $("#alert-type").text(alertType);
    $("#alert-message").text(alertMessage);
}

$(".btn-close").on("click", function () {
    $("#custom-alert").html("");
});

$(document).on("click", "#reset", function () {
	$("#age, #sex, #children, #region, #smoker, #bmi, #result").val("");
});

$(document).on("click", "#submit", function () {
    let data = null;
    operation = this.id;
    if (operation === "submit") {
        data = JSON.stringify({
            age: parseInt($("#age").val()),
            sex: $("#sex").val(),
            bmi: parseFloat($("#bmi").val()),
            children: parseInt($("#children").val()),
            smoker: $("#smoker").val(),
            region: $("#region").val()
        });
        console.log(data);
        create(data);
    }
});

function create(data = null) {
    if (
        $("#age").val() &&
        $("#sex").val() &&
        $("#bmi").val() &&
        $("#children").val() &&
        $("#smoker").val() &&
        $("#region").val()
    ) {
        $.ajax({
            url: "http://127.0.0.1:5000/insurance/predict",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (result) {
                console.log(result);
                if (result.status) {
                    setAlert(
                        "alert alert-success alert-dismissible fade show",
                        "Success!!!",
                        "Prediction Successful!"
                    );
                    $("#h5").prop("hidden", false);
                    $("#result").prop("hidden", false);
                    $("#result").val(result.predicted_charges_value)
                } else {
                    setAlert(
                        "alert alert-danger alert-dismissible fade show",
                        "Warning!!!",
                        "Invalid input Parameters, please provide proper values!"
                    );
                }
            },
            error: function (err) {
                console.log(err);
            },
            complete: function () {
            },
        });
    } else {
        setAlert(
            "alert alert-danger alert-dismissible fade show",
            "Warning!!!",
            "Please give values to all input field!"
        );
    }
}