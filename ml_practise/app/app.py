from flask import Flask, render_template, request
from ml_practise.app import request_wrapper
from ml_practise.runner.initializer import Initializer
from ml_practise import config

app = Flask(__name__)
initializer = Initializer()


@app.route("/insurance")
def home_page():
    return render_template("index.html")


@app.route("/insurance/train", methods=["GET"])
@request_wrapper
def train_model():
    score, status = initializer.train()
    new_data = {
        "best_score": score,
        "status": status,
    }
    return new_data


@app.route("/insurance/predict", methods=["POST"])
@request_wrapper
def predict_charges():

    data = request.get_json()
    pred_val, status = initializer.predict(data)
    new_data = {
        "predicted_charges_value": pred_val,
        "status": status,
    }
    data.update(new_data)
    return data


if __name__ == "__main__":
    app.run(
        host=config['FLASK_HOST'],
        port=config['FLASK_PORT'],
        debug=config['FLASK_DEBUG']
    )
