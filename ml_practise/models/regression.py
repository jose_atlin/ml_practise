import pickle
from typing import Dict, Tuple
import optuna
import pandas as pd
from sklearn.linear_model import Lasso, LinearRegression
from sklearn import metrics
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.pipeline import Pipeline
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor

from ml_practise.data_processing.pipeline import Pipelining
from ml_practise.data_processing.preprocessing import PreProcessing
from ml_practise.mlflow.mlflow_dto import MlflowDTO
from ml_practise.data_processing.utils import Utils
from ml_practise.models import path
from ml_practise import config


class Regression:

    REGRESSORS = [
        (LinearRegression, "lr_objective"),
        (DecisionTreeRegressor, "dtr_objective"),
        (RandomForestRegressor, "rfr_objective"),
        (SVR, "svr_objective"),
        (Lasso, "las_objective"),
    ]

    def __init__(self, data: pd.DataFrame, target: str) -> None:
        self.data = data
        self.target = target
        self.preprocessing = PreProcessing(data, target)
        self.pipelining = Pipelining()
        self.preprocessor = None
        self.utils = Utils()
        self.best_model = None
        self.mlflow_dto = MlflowDTO()

    def mlflow_logger(self):
        experiment_id = f"{config['DATASET']}_dataset"
        self.mlflow_dto.set_experiment_id(experiment_id)
        ml_lib = "sklearn"
        self.mlflow_dto.set_ml_lib(ml_lib)
        self.mlflow_dto.log_to_mlflow()

    def train_model(self, model) -> metrics.r2_score:
        X_train, X_test, y_train, y_test = self.preprocessing.train_test_split()
        pipe = Pipeline(
            [
                ("preprocessor", self.preprocessor),
                ("regressor", model),
            ]
        )

        pipe.fit(X_train, y_train)
        y_predicted = pipe.predict(X_test)
        return metrics.r2_score(y_test, y_predicted)

    def detailed_objective(self, regressor, params: Dict):
        X_train, X_test, y_train, y_test = self.preprocessing.train_test_split()
        pipe = Pipeline(
            [
                ("preprocessor", self.preprocessor),
                ("regressor", regressor(**params)),
            ]
        )

        pipe.fit(X_train, y_train)
        regressor: str = (type(regressor()).__name__).lower()
        with open(
            path
            / f"estimation/{config['DATASET']}_{regressor}_final_model.pkl",
            "wb",
        ) as pickle_model:
            pickle.dump(pipe, pickle_model)
        y_predicted = pipe.predict(X_test)
        self.utils.view_model(y_test, y_predicted, regressor)
        self.mlflow_dto.add_artifact_path(
            path / f"estimation/{regressor}_view_model.png"
        )
        metrics = {
            "mse": mean_squared_error(y_test, y_predicted),
            "mae": mean_absolute_error(y_test, y_predicted),
            "r2_score": r2_score(y_test, y_predicted),
            "rmse": mean_squared_error(y_test, y_predicted, squared=False),
        }
        self.mlflow_dto.set_metrics(metrics)
        self.mlflow_dto.set_params(params)

        self.mlflow_dto.add_model(
            {
                "local_path": path
                / f"estimation/{config['DATASET']}_{regressor}_final_model.pkl",
                "artifact_path": f"{config['DATASET']}_{regressor}_train_model",
                "registered_model_name": f"{config['DATASET']}_{regressor}_train_model",
            }
        )
        self.mlflow_logger()

    def lr_objective(self, trial):
        model = LinearRegression()
        return self.train_model(model)

    def rfr_objective(self, trial):
        rfr_params = {
            "max_depth": trial.suggest_int("max_depth", 20, 100),
            "max_features": trial.suggest_int("max_features", 2, 10),
            "min_samples_leaf": trial.suggest_int("min_samples_leaf", 2, 10),
            "min_samples_split": trial.suggest_int("min_samples_split", 2, 10),
            "n_estimators": trial.suggest_int("n_estimators", 100, 500),
        }
        model = RandomForestRegressor(**rfr_params)
        return self.train_model(model)

    def dtr_objective(self, trial):
        dtr_params = {
            "max_depth": trial.suggest_int("max_depth", 10, 100),
            "max_features": trial.suggest_int("max_features", 2, 10),
            "min_samples_leaf": trial.suggest_int("min_samples_leaf", 2, 10),
            "min_samples_split": trial.suggest_int("min_samples_leaf", 2, 10),
        }
        model = DecisionTreeRegressor(**dtr_params)
        return self.train_model(model)

    def svr_objective(self, trial):
        svr_params = {
            "kernel": trial.suggest_categorical("kernel", ["poly"]),
            "C": trial.suggest_float("C", 1e-1, 1e1, log=True),
            "gamma": trial.suggest_float("gamma", 1e-1, 1e1, log=True),
            "epsilon": trial.suggest_float("epsilon", 1e-1, 1e1, log=True),
        }
        model = SVR(**svr_params)
        return self.train_model(model)

    def las_objective(self, trial):
        las_params = {
            "alpha": trial.suggest_float("alpha", 0, 1, step=0.1),
        }
        model = Lasso(**las_params)
        return self.train_model(model)

    def plot_optuna(self, regressor, study):
        fig = optuna.visualization.matplotlib.plot_optimization_history(
            study
        ).get_figure()
        path_to_file = (
            path / f"estimation/{regressor}_plot_optimization_history.png"
        )
        fig.savefig(path_to_file)
        self.mlflow_dto.add_artifact_path(path_to_file)

        fig = optuna.visualization.matplotlib.plot_param_importances(
            study
        ).get_figure()
        path_to_file = (
            path / f"estimation/{regressor}_plot_param_importances.png"
        )
        fig.savefig(path_to_file)
        self.mlflow_dto.add_artifact_path(path_to_file)

    def evaluate_trials(self, regressor, trials):
        params = [trials[i].params for i in range(len(trials))]
        path_to_file = path / f"estimation/{regressor}_estimation_trials.csv"
        pd.DataFrame(params).to_csv(path_to_file)
        self.mlflow_dto.add_artifact_path(path_to_file)

    def study_models(self, regressor, objective):
        self.mlflow_dto = MlflowDTO()
        study = optuna.create_study(direction="maximize")
        study.optimize(objective, n_trials=100)
        trials = study.get_trials()
        regressor_str = (type(regressor()).__name__).lower()
        self.evaluate_trials(regressor_str, trials)
        self.plot_optuna(regressor_str, study)
        self.detailed_objective(regressor, study.best_params)

    def train(self) -> Tuple:
        self.preprocessor = self.pipelining.regression_pipeline(
            self.preprocessing.get_numerical_cols(),
            self.preprocessing.get_categorical_cols(),
        )
        for regressor, objective in self.REGRESSORS:
            objective = getattr(self, objective)
            self.study_models(regressor, objective)
        return 1, True

    def predict(self, X_new: Dict) -> Tuple:
        X_new = self.preprocessing.convert_to_pd_df(X_new)

        # loading model from logged model to mlflow registry
        model_name = f"{config['DATASET']}_{config['REGRESSOR']}_train_model"
        model_version = 1
        model_uri = f"models:/{model_name}/{model_version}"
        loaded_model = self.mlflow_dto.load_from_registry(model_uri=model_uri)
        pred_val = loaded_model.predict(X_new)[0]
        print("Model prediction successful!")
        return pred_val, True

        # loading model from logged mlflow model
        # model_uri = f"runs:/{config['RUN_ID']}/{config['DATASET']}_{config['REGRESSOR']}_train_model"
        # loaded_model = self.mlflow_dto.load_from_mlflow(model_uri=model_uri)
        # pred_val = loaded_model.predict(X_new)[0]
        # print("Model prediction successful!")
        # return pred_val, True
