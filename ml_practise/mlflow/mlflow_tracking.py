import pickle
from typing import Dict
import mlflow


class MlflowTracking:

    ml_libraries = {
        "sklearn": mlflow.sklearn,
        "tensorflow": mlflow.tensorflow,
        "pytorch": mlflow.pytorch,
        "keras": mlflow.keras,
    }

    def __init__(self) -> None:
        pass

    def log_to_mlflow(
        self,
        experiment_id: str,
        ml_lib: str,
        model: Dict,
        artifact_paths: Dict,
        params: Dict,
        metrics: Dict
    ):
        mlflow.set_tracking_uri("http://localhost:5000")
        mlflow.set_experiment(experiment_id)
        with mlflow.start_run():

            with open(model['local_path'], "rb") as pickle_model:
                regressor = pickle.load(pickle_model)
            self.ml_libraries[ml_lib].log_model(
                regressor,
                artifact_path=model['artifact_path'],
                registered_model_name=model['registered_model_name']
            )

            for artifact in artifact_paths:
                mlflow.log_artifact(local_path=artifact)

            if params:
                mlflow.log_params(params=params)
            if metrics:
                mlflow.log_metrics(metrics=metrics)          

    def load_from_mlflow(self, model_uri: str):
        loaded_model = mlflow.pyfunc.load_model(
            model_uri=model_uri
        )
        return loaded_model
    
    def load_from_registry(self, model_uri: str):
        mlflow.set_tracking_uri("http://localhost:5000")
        model = mlflow.pyfunc.load_model(model_uri=model_uri)
        return model

# mlflow server --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./artifacts --host 0.0.0.0
# mlflow models serve -m "models:/sk-learn-random-forest-reg-model/Production"
