from typing import Dict, Tuple, Union
from ml_practise.mlflow.mlflow_tracking import MlflowTracking


class MlflowDTO:

    def __init__(self) -> None:
        self.mlflow_tracking = MlflowTracking()
        self.experiment_id = None
        self.ml_lib = None
        self.artifact_paths = []
        self.model = {}
        self.params = {}
        self.metrics = {}

    def set_experiment_id(self, experiment_id: str):
        self.experiment_id = experiment_id
    
    def set_ml_lib(self, ml_lib: str):
        self.ml_lib = ml_lib
    
    def add_artifact_path(self, artifact_path: str):
        self.artifact_paths.append(artifact_path)

    def add_model(self, model: Dict):
        self.model = model
    
    def set_params(self, params: Union[Dict, Tuple]):
        if isinstance(params, dict):
            self.params.update(params)
        else:
            self.params[params[0]] = params[1]
    
    def set_metrics(self, metrics: Union[Dict, Tuple]):
        if isinstance(metrics, dict):
            self.metrics.update(metrics)
        else:
            self.metrics[metrics[0]] = metrics[1]

    def validate_params(self):
        print(
            f"experiment id: {self.experiment_id}\n"
            f"ml_lib: {self.ml_lib}"
        )
        if any((self.experiment_id, self.ml_lib)) is None:
            return False

        print(
            f"local_path: {self.model['local_path']}\n"
            f"artifact_path: {self.model['artifact_path']}\n"
            f"registered_model_name: {self.model['registered_model_name']}"
        )
        if not self.model['local_path'].is_file():
            return False

        for artifact in self.artifact_paths:
            print(f"local_path: {artifact}")
            if not (artifact).is_file():
                self.artifact_paths.remove(artifact)
        return True

    def log_to_mlflow(self):
        validation = self.validate_params()
        if validation == True:
            self.mlflow_tracking.log_to_mlflow(
                experiment_id=self.experiment_id,
                ml_lib=self.ml_lib,
                model=self.model,
                artifact_paths=self.artifact_paths,
                params=self.params,
                metrics=self.metrics
            )

    def load_from_mlflow(self, model_uri: str):
        return self.mlflow_tracking.load_from_mlflow(model_uri)

    def load_from_registry(self, model_uri: str):
        return self.mlflow_tracking.load_from_registry(model_uri)
