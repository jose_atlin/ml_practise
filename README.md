## ML Practise
This is an ML Framework which incorporated different ML Models using scikit-learn library.

-----

##### Installing package :

- download / clone the repository.

- move to the cloned local repository OR specify the path to the cloned repository as the end of the below package installation code.

- run command:

for installing the package in editable mode, do:
 ```
 python -m pip install -e .
 ```

 for installing the package in editable mode with specific profile, do:
 > for eg: for dev profile
 ```
 python -m pip install -e .[dev]
 ```

 for installing the package without editable mode, do:
 ```
 python -m pip install .
 ```

-----

##### How to Use :
- To start the flask server, run ```python run.py``` in app folder.
- Once the server is up, you can do training / prediction.
- ***NOTE***: To do prediction, training of the model needs to be done atleast once, which then will be pickled to final_model.pkl file.
- To do training: run ```python train.py```.
- To do predicting: run ```python predict.py```.
- Best model for the dataset will be pickled.
- All the trained model test score, train score etc.. will be available at estimator_values.csv.
- the predicted vs actual target value plot will be store in estimation/view_model.png.

-----

##### Contents :

Included Regression Models:
- **Linear Regression**
- **Decision Tree Regression**
- **Random Forest Regression**
- **Lasso Regression**
- **Support Vector Regression**

-----

> New Models will be added in the successive releases..

scikit-learn official documentation: [scikit-learn documentation](https://scikit-learn.org/stable/)
