import os
from setuptools import setup
import pkg_resources
import pathlib


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with pathlib.Path("requirements.txt").open() as requirements_txt:
    install_requires = [
        str(requirement)
        for requirement in pkg_resources.parse_requirements(requirements_txt)
    ]

setup(
    name="ml_practise",
    version="0.0.1a",
    description="""A Framework for all ML Models""",
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    url="git@bitbucket.org:jose_atlin/ml_practise.git",
    author="Jose Atlin",
    author_email="jose.atlin@litmus7.com",
    packages=["ml_practise"],
    python_requires=">=3.7",
    install_requires=install_requires,
    extras_require={
        "dev": ["flake8 == 4.0.1", "black == 22.1.0", "pre-commit == 2.17.0"],
    },
)
